package model.Implementations;

import model.Interfaces.Cell;

public class PrisonCellFactory implements model.Interfaces.CellFactory {

    private enum CellFloor {
        FIRST_FLOOR ("Primo piano", 4),
        SECOND_FLOOR ("Secondo piano", 3),
        THIRD_FLOOR ("Terzo piano", 4),
        BASEMENT ("Piano sotteraneo, celle di isolamento", 1);

        private final String position;
        private final int capacity;

        CellFloor(String position, int capacity) {
            this.position = position;
            this.capacity = capacity;
        }

        private String position() {
            return this.position;
        }

        private int capacity() {
            return this.capacity;
        }
    }

    public Cell getCell(int id) {
        if (id < 20) {
            return new CellImpl(id, CellFloor.FIRST_FLOOR.position(), CellFloor.FIRST_FLOOR.capacity());
        } else if (id < 40) {
            return new CellImpl(id, CellFloor.SECOND_FLOOR.position(), CellFloor.SECOND_FLOOR.capacity());
        } else if (id < 45) {
            return new CellImpl(id, CellFloor.THIRD_FLOOR.position(), CellFloor.THIRD_FLOOR.capacity());
        } else {
            return new CellImpl(id, CellFloor.BASEMENT.position(), CellFloor.BASEMENT.capacity());
        }
    }
}
