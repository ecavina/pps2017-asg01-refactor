package model.Implementations;

import java.util.Date;

public class GuardImplBuilder {
    private String name;
    private String surname;
    private Date birthDate;
    private int rank;
    private String telephoneNumber;
    private int idGuardia;
    private String password;

    public GuardImplBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public GuardImplBuilder setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public GuardImplBuilder setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public GuardImplBuilder setRank(int rank) {
        this.rank = rank;
        return this;
    }

    public GuardImplBuilder setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
        return this;
    }

    public GuardImplBuilder setIdGuardia(int idGuardia) {
        this.idGuardia = idGuardia;
        return this;
    }

    public GuardImplBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public GuardImpl createGuardImpl() {
        return new GuardImpl(name, surname, birthDate, rank, telephoneNumber, idGuardia, password);
    }
}