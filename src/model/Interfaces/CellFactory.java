package model.Interfaces;

/**
 * Pattern Abstract Factory
 */
public interface CellFactory {

    /**
     * Return a specific cell
     * @param id
     * @return Cell an implementation of a Cell with all the informations
     */
    Cell getCell(int id);

}
