package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import model.Interfaces.Guard;
import view.Interfaces.LoginView;
import view.Interfaces.MainView;

/**
 * controller della login view
 */
public class LoginControllerImpl {

    private static final String SIGN_IN_MESSAGE = "accedere con i profili:";
    private static final String FIRST_USER_DATA = "id:3 , password:qwerty";
    private static final String SECOND_USER_DATA = "id:2 , password:asdasd";
    private final static String SAVING_DIR = "res";
    private final static String GUARDS_CONFIGURATION_FILE = SAVING_DIR + "/GuardieUserPass.txt";
    private final static String WRONG_USER_PASSWORD = "Combinazione username/password non corretta";
    private final static String EMPTY_USER_PASSWORD = "Devi inserire username e password";

    private LoginView loginView;

    /**
     * costruttore
     *
     * @param loginView la view
     */
    public LoginControllerImpl(LoginView loginView) throws IOException, ClassNotFoundException {
        this.loginView = loginView;
        this.loginView.addLoginListener(new LoginListener(getGuards()));
        this.loginView.displayErrorMessage(initialMessage());
    }

    private String initialMessage() {
        StringBuilder sb = new StringBuilder();
        sb.append(SIGN_IN_MESSAGE);
        sb.append(System.lineSeparator());
        sb.append(FIRST_USER_DATA);
        sb.append(System.lineSeparator());
        sb.append(SECOND_USER_DATA);
        return sb.toString();
    }

    /**
     * listener che si occupa di effettuare il login
     */
    public class LoginListener implements ActionListener {

        private List<Guard> guardsList;

        LoginListener(List<Guard> guards) {
            this.guardsList = guards;
        }

        @Override
        public void actionPerformed(ActionEvent arg0) {
            boolean isInside = false;

            if (loginView.getUsername().isEmpty() || loginView.getPassword().isEmpty()) {
                loginView.displayErrorMessage(EMPTY_USER_PASSWORD);
            } else {
                for (Guard g : this.guardsList) {
                    if (loginView.getUsername().equals(String.valueOf(g.getUsername()))
                            && loginView.getPassword().equals(g.getPassword())) {
                        isInside = true;
                        loginView.displayErrorMessage("Benvenuto Utente " + loginView.getUsername());
                        loginView.dispose();
                        new MainControllerImpl(new MainView(g.getRank()));
                    }
                }
                if (!isInside) {
                    loginView.displayErrorMessage(WRONG_USER_PASSWORD);
                }
            }
        }
    }

    /**
     * metodo che restutuisce la lista di guardie salvata su file
     *
     * @return lista di guardie
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static List<Guard> getGuards() throws IOException, ClassNotFoundException {
        File f = new File(GUARDS_CONFIGURATION_FILE);
        List<Guard> guards = new ArrayList<>();
        if (f.length() != 0) {
            FileInputStream fi = new FileInputStream(f);
            ObjectInputStream oi = new ObjectInputStream(fi);

            try {
                while (true) {
                    Guard s = (Guard) oi.readObject();
                    guards.add(s);
                }
            } catch (EOFException eofe) {
            }

            fi.close();
            oi.close();
        }
        return guards;
    }
}
