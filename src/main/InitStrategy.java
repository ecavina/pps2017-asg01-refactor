package main;

import java.io.File;
import java.io.IOException;

/**
 *
 */
public interface InitStrategy {

    void init(File file) throws IOException;
}
