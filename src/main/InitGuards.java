package main;

import model.Implementations.GuardImpl;
import model.Implementations.GuardImplBuilder;
import model.Interfaces.Guard;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InitGuards implements InitStrategy {

    private final static String DATE_PATTERN = "MM/dd/yyyy";
    private final static String DEFAULT_DATE = "01/01/1980";

    @Override
    public void init(File file) {
        try {
            initializeGuards(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * metodo che inizializza le guardie
     *
     * @param fg file in cui creare le guardie
     * @throws IOException
     */
    private static void initializeGuards(File fg) throws IOException {

        List<Guard> guardsList = createGuardsList(getDefaultDate());

        FileOutputStream fo = new FileOutputStream(fg);
        ObjectOutputStream os = new ObjectOutputStream(fo);
        os.flush();
        fo.flush();
        for (Guard g : guardsList) {
            os.writeObject(g);
        }
        os.close();
    }

    private static Date getDefaultDate() {
        SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);
        Date date = new Date();
        try {
            date = format.parse(DEFAULT_DATE);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private static List<Guard> createGuardsList(Date date) {
        List<Guard> list = new ArrayList<>();
        GuardImpl g1 = new GuardImplBuilder()
                .setName("Oronzo")
                .setSurname("Cantani")
                .setBirthDate(date)
                .setRank(1)
                .setTelephoneNumber("0764568")
                .setIdGuardia(1)
                .setPassword("ciao01")
                .createGuardImpl();
        GuardImpl g2 = new GuardImplBuilder().
                setName("Emile")
                .setSurname("Heskey")
                .setBirthDate(date)
                .setRank(2)
                .setTelephoneNumber("456789")
                .setIdGuardia(2)
                .setPassword("asdasd")
                .createGuardImpl();
        GuardImpl g3 = new GuardImplBuilder()
                .setName("Gennaro")
                .setSurname("Alfieri")
                .setBirthDate(date)
                .setRank(3)
                .setTelephoneNumber("0764568")
                .setIdGuardia(3)
                .setPassword("qwerty")
                .createGuardImpl();

        list.add(g1);
        list.add(g2);
        list.add(g3);
        return list;
    }
}
