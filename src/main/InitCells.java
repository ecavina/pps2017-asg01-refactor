package main;

import model.Implementations.PrisonCellFactory;
import model.Interfaces.Cell;
import model.Interfaces.CellFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class InitCells implements InitStrategy {

    private final static int CELLS_NUMBER = 50;

    @Override
    public void init(File file) throws IOException {
        initializeCells(file);
    }

    /**
     * metodo che inizializza le celle
     *
     * @param f file in cui creare le celle
     * @throws IOException
     */
    private static void initializeCells(File f) throws IOException {
        List<Cell> cellsList = createCellsList();

        FileOutputStream fo = new FileOutputStream(f);
        ObjectOutputStream os = new ObjectOutputStream(fo);
        os.flush();
        fo.flush();
        for (Cell c1 : cellsList) {
            os.writeObject(c1);
        }
        os.close();
    }

    private static List<Cell> createCellsList() {
        CellFactory factory = new PrisonCellFactory();
        List<Cell> list = new ArrayList<>();
        IntStream.range(BigDecimal.ZERO.intValue(), CELLS_NUMBER)
                .forEach(i -> list.add(factory.getCell(i)));
        return list;
    }
}
