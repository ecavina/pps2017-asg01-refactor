package main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

import controller.Implementations.LoginControllerImpl;
import model.Implementations.GuardImplBuilder;
import model.Implementations.PrisonCellFactory;
import model.Implementations.GuardImpl;
import model.Interfaces.Cell;
import model.Interfaces.CellFactory;
import model.Interfaces.Guard;
import view.Interfaces.LoginView;

/**
 * The main of the application.
 */
public final class Main {

    private final static String SAVING_DIR = "res";
    private final static String GUARDS_CONFIGURATION_FILE = SAVING_DIR + "/GuardieUserPass.txt";
    private final static String CELLS_CONFIGURATION_FILE = SAVING_DIR + "/Celle.txt";

    /**
     * Program main, this is the "root" of the application.
     *
     * @param args unused,ignore
     */
    public static void main(final String... args) throws IOException, ClassNotFoundException {
        createDir();
        initIfNecessary(GUARDS_CONFIGURATION_FILE, new InitGuards());
        initIfNecessary(CELLS_CONFIGURATION_FILE, new InitCells());

        new LoginControllerImpl(new LoginView());
    }

    private static void initIfNecessary(final String path, InitStrategy initStrategy) {
        final File file = new File(path);
        if (!file.exists()) {
            try {
                initStrategy.init(file);
            } catch (IOException e) {
                e.printStackTrace();  // Technical debt ...
            }
        }
    }

    private static void createDir() {
        Path path = Paths.get(Main.SAVING_DIR);
        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                e.printStackTrace(); // technical debt
            }
        }
    }
}
